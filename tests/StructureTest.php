<?php

use Insidesuki\EntityMapping\Structure;
use PHPUnit\Framework\TestCase;

class StructureTest extends TestCase
{


	protected $xml;

	public function setUp(): void
	{
		$this->xml = __DIR__ . '/../src/Example/Mapping/ClienteVo.orm.xml';
	}

	public function testFailXmlDoesNotExists()
	{

		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('XmlMapping not found!!');
		new Structure( __DIR__ . '/Example/Maing/ClienteVo.orm.xml');

	}


	public function testFieldsWasCreated(){

		$structure = new Structure($this->xml);
		$structure();
		$this->assertIsArray($structure->columnsMapped());


	}

}
