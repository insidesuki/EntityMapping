<?php

use Insidesuki\DDDUtils\Domain\ValueObject\Nif;
use Insidesuki\EntityMapping\EntityMapping;
use Insidesuki\EntityMapping\Example\Command\ClienteCommand;
use Insidesuki\EntityMapping\Example\Command\ClienteVoCommand;
use Insidesuki\EntityMapping\Example\Domain\Entity\Cliente;
use Insidesuki\EntityMapping\Example\Domain\Entity\ClienteWithVo;
use PHPUnit\Framework\TestCase;

class EntityMappingTest extends TestCase
{
	public function testFailMappingDoesNotExists(){

		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('XmlMapping not found!!');
		$em = new EntityMapping(
			__DIR__.'/../../src/Example/Mappinsg/Cliente.orm.xml',
			ClienteCommand::class
		);

	}

	public function testFailCommandDoesNotExists(){

		$this->expectException(InvalidArgumentException::class);
		$this->expectExceptionMessage('Command class not exists!!');
		$em = new EntityMapping(
			__DIR__ . '/../src/Example/Mapping/ClienteVo.orm.xml',
			'sssdssd'
		);

	}

	public function testFailCommandNotImplementsMappeableInterface(){

		$this->expectException(RuntimeException::class);
		$this->expectExceptionMessage(sprintf('The %s, must implement 
			Insidesuki\SolApiManager\Contracts\MappeableInterface',DummyCommand::class));
		$em = new EntityMapping(
			__DIR__.'/../src/Example/Mapping/Cliente.orm.xml',
			DummyCommand::class
		);

	}


	public function testMapCommandToString(){
		$em = new EntityMapping(
			__DIR__.'/../src/Example/Mapping/Cliente.orm.xml',
			ClienteCommand::class
		);
		$em->mapCommandToString(false);
		$this->assertFalse($em->mapToStringStatus());


	}


	public function testOkMappingFromArray(){

		$em = new EntityMapping(
			__DIR__.'/../src/Example/Mapping/Cliente.orm.xml',
			ClienteCommand::class
		);

		$data = [
			'CODCLI' => 'asdasdAAAAA',
			'NIFCLI' => 'x875456545x',
			'NOFCLI' => 'nuevo cliente'
		];
		$client = $em->toEntity($data);
		$this->assertInstanceOf(Cliente::class,$client);
		$this->assertSame($data['CODCLI'], $client->codigo());
		$this->assertSame($data['NIFCLI'],$client->cif());
		$this->assertSame($data['NOFCLI'],$client->nombre());

	}




	public function testToEntityWithEmbedable(){

		$em = new EntityMapping(
			__DIR__.'/../src/Example/Mapping/ClienteVo.orm.xml',
			ClienteVoCommand::class
		);

		$data = [
			'CODCLI' => 'asdasdAAAAA',
			'NIFCLI' => 'X9774196R',
			'NOFCLI' => 'nuevo cliente'
		];
		$client = $em->toEntity($data);

		$this->assertInstanceOf(ClienteWithVo::class,$client);
		$this->assertInstanceOf(Nif::class,$client->nif());


	}

	public function testCreateArrayFromEntity(){

		$em = new EntityMapping(
			__DIR__.'/../src/Example/Mapping/ClienteVo.orm.xml',
			ClienteVoCommand::class
		);

		$commandVo = new ClienteVoCommand();
		$commandVo->setNombre('uno');
		$commandVo->setCodigo('123123');
		$commandVo->setNif('X9774196R');
		$clientEntity = ClienteWithVo::create($commandVo);

		$this->assertInstanceOf(ClienteWithVo::class,$clientEntity);

		$array = $em->toArray($clientEntity);


		$this->assertSame($clientEntity->nif()->nif,$array['NIFCLI']);
		$this->assertSame($clientEntity->nombre(),$array['NOFCLI']);
		$this->assertSame($clientEntity->codigo(),$array['CODCLI']);
		$this->assertSame($clientEntity->nif()->type,$array['NIFTYPE']);




	}


}


class DummyCommand{

}
