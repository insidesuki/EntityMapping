**EntityMapping**
==============================

Mapping for entities, when doctrine is not possible to use, (api mappings, etc)

**Features**
==

- Support entity to array
- Array to entity
- Embeddables
- 100% compatibility with doctrine xml mapping