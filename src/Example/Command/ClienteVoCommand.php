<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Example\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Insidesuki\EntityMapping\Contracts\MappeableInterface;


class ClienteVoCommand extends DtoSerializer implements MappeableInterface
{

	protected string $codigo;
	protected string $nombre;
	protected string $nif;

	public function getCodigo(): string
	{
		return $this->codigo;
	}

	public function setCodigo(string $codigo): void
	{
		$this->codigo = $codigo;
	}

	public function getNombre(): string
	{
		return $this->nombre;
	}

	public function setNombre(string $nombre): void
	{
		$this->nombre = $nombre;
	}

	public function getCif(): string
	{
		return $this->nif;
	}

	public function setNif(string $nif): void
	{
		$this->nif = $nif;
	}





}