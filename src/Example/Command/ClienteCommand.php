<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Example\Command;

use Insidesuki\DDDUtils\Domain\DtoSerializer;
use Insidesuki\EntityMapping\Contracts\MappeableInterface;

class ClienteCommand extends DtoSerializer implements MappeableInterface
{

	protected string $codigo;
	protected string $nombre;
	protected string $cif;

	public function getCodigo(): string
	{
		return $this->codigo;
	}

	public function setCodigo(string $codigo): void
	{
		$this->codigo = $codigo;
	}

	public function getNombre(): string
	{
		return $this->nombre;
	}

	public function setNombre(string $nombre): void
	{
		$this->nombre = $nombre;
	}

	public function getCif(): string
	{
		return $this->cif;
	}

	public function setCif(string $cif): void
	{
		$this->cif = $cif;
	}





}