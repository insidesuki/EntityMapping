<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Example\Domain\Entity;

class Cliente
{

	protected string $codigo;
	protected string $nombre;
	protected string $cif;


	private function __construct(string $codigo, string $nombre, string $cif)
	{

		$this->codigo = $codigo;
		$this->nombre = $nombre;
		$this->cif    = $cif;
	}

	public static function create($command): self
	{

		return new self(
			$command->getCodigo(),
			$command->getNombre(),
			$command->getCif()
		);

	}
	public function nombre(): string
	{
		return $this->nombre;
	}


	public function codigo(): string
	{
		return $this->codigo;
	}


	public function cif(): string
	{
		return $this->cif;
	}


}