<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Example\Domain\Entity;

use Insidesuki\DDDUtils\Domain\ValueObject\Nif;

class ClienteWithVo
{

	protected string $codigo;
	protected string $nombre;
	protected Nif    $nif;


	private function __construct(
		string $codigo,
		string $nombre,
		Nif $nif
	)
	{

		$this->codigo = $codigo;
		$this->nombre = $nombre;
		$this->nif    = $nif;
	}

	public static function create($command): self
	{

		return new self(
			$command->getCodigo(),
			$command->getNombre(),
			Nif::createAndValidate($command->getCif())
		);

	}

	public function nombre(): string
	{
		return $this->nombre;
	}


	public function codigo(): string
	{
		return $this->codigo;
	}


	public function nif(): Nif
	{
		return $this->nif;
	}


}