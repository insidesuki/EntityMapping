<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping;
use Insidesuki\EntityMapping\Contracts\MappeableInterface;
use InvalidArgumentException;
use RuntimeException;

class EntityCreator
{


	protected bool $mapAllToString = true;

	public function __construct(private readonly Structure $structure){}

	public function create(array $data, MappeableInterface $commandFqns, string $creationMethod = 'create'): object
	{

		$columns = $this->structure->columnsMapped();

		$dataCreate = [];
		foreach ($data as $field => $value) {

			if(is_array($field)) {
				throw new InvalidArgumentException('Cant map a field when is a array');
			}
			// check if exists in mapping array
			$existsField = $this->structure->searchColumn($field);

			if(null !== $existsField){
				$dataCreate[$existsField->name] = $value;
			}

		}

		// create command class
		$command = new $commandFqns;
		// parse all to string
		$data = ($this->mapAllToString) ? array_map('strval', $dataCreate) : $dataCreate;
		$command->populate($data);

		if(!method_exists($command, 'populate')) {

			throw new RuntimeException('Populate method not exists in %s. Ensure that implements MappeableInterface  ');

		}
		$entity = $this->structure->entity();
		return $entity::$creationMethod($command);


	}

}