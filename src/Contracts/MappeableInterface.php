<?php

namespace Insidesuki\EntityMapping\Contracts;

interface MappeableInterface
{

	public function populate(array $data);

}