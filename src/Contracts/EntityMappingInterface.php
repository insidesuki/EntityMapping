<?php

namespace Insidesuki\EntityMapping\Contracts;

interface EntityMappingInterface
{

	public function toArray(object $entity): array;

	public function parseDataToEntityCollection(array $dataCollection):array;

	public function toEntity(array $data): object;

}