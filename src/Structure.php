<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping;

use Insidesuki\EntityMapping\Embedded\EmbeddableXtractor;
use Insidesuki\EntityMapping\Embedded\Embedded;
use Insidesuki\EntityMapping\Fields\Field;
use Insidesuki\EntityMapping\Fields\FieldXtractor;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;

class Structure
{

	protected ?string       $entityNameFqns;
	private readonly string $entityKey;
	private Crawler         $crawler;
	private array           $columnsMapped;
	private array           $embeddables =[];

	public function __construct(private readonly string $mappingXml)
	{
		if(!file_exists($mappingXml)) {
			throw new InvalidArgumentException('XmlMapping not found!!');
		}
	}

	public function __invoke()
	{

		$this->crawler        = new Crawler(file_get_contents($this->mappingXml));
		// extract al fields
		$xtractor = new FieldXtractor($this);
		$xtractor();
		// extract all embeddables
		$embedableXtractor = new EmbeddableXtractor($this);
		$embedableXtractor();
		$this->setEntity();;


	}

	private function setEntity(): void
	{


		$entityNameFqns = $this->crawler->filter('entity')->attr('name');
		if(!class_exists($entityNameFqns)) {
			throw new RuntimeException(sprintf('Not found entityFqns %s, defined in yout orm.mapping', $entityNameFqns));
		}

		$this->entityNameFqns = $entityNameFqns;

	}

	public function searchColumn(string $column): ?Field
	{
		$match = null;
		foreach ($this->columnsMapped as $field) {
			if($field->column === $column) {
				$match = $field;
				break;
			}
		}
		return $match;

	}

	public function entity(): string
	{
		return $this->entityNameFqns;
	}

	public function columnsMapped()
	{
		return $this->columnsMapped;
	}

	public function embeddables(): array
	{
		return $this->embeddables;
	}

	public function addId(string $id): void
	{
		$this->entityKey = $id;
	}

	public function entityKey(): string
	{
		return $this->entityKey;
	}

	public function addToColumnsMapped(Field $field): void
	{
		$this->columnsMapped[] = $field;
	}

	public function addToEmbeddable(Embedded $em)
	{
		$this->embeddables[] = $em;
	}

	public function crawler(): Crawler
	{

		return $this->crawler;
	}


}