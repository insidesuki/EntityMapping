<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping;

use Insidesuki\EntityMapping\Contracts\EntityMappingInterface;
use Insidesuki\EntityMapping\Contracts\MappeableInterface;
use InvalidArgumentException;
use RuntimeException;

class EntityMapping implements EntityMappingInterface
{

	protected           $mappingXml;
	protected Structure $structure;
	private string      $entityNameFqns;
	private object      $commandFqns;

	private bool    $mapAllToString = true;


	/**
	 * If your entity constructor is private
	 * @var bool
	 */
	private bool $privateConstructor = true;

	private string $creationMethod = 'create';

	public function __construct(string $mappingXml, string $commandFqns)
	{

		$this->setMappingFile($mappingXml);
		$this->setCommand($commandFqns);


	}

	private function setMappingFile($mappingXml)
	{
		if(!file_exists($mappingXml)) {
			throw new InvalidArgumentException('XmlMapping not found!!');
		}
		$this->mappingXml = $mappingXml;
	}

	private function setCommand(string $commandFqns)
	{
		if(!class_exists($commandFqns)) {
			throw new InvalidArgumentException('Command class not exists!!');
		}
		$this->commandFqns = new $commandFqns;
		if(!$this->commandFqns instanceof MappeableInterface) {
			throw new RuntimeException(sprintf('The %s, must implement 
			Insidesuki\SolApiManager\Contracts\MappeableInterface', $commandFqns));
		}
	}

	public function mapCommandToString(bool $map): void
	{
		$this->mapAllToString = $map;
	}

	public function toArray(object $entity): array
	{

		$this->parseXmlMappingStructure();
		return (new EntityArrayCreator($this->structure))->create($entity);

	}

	private function parseXmlMappingStructure()
	{

		$structure = new Structure($this->mappingXml);
		$structure();
		$this->structure = $structure;


	}



	public function parseDataToEntityCollection(array $dataCollection): array
	{


		unset($dataCollection['total']);
		$collection = [];
		foreach ($dataCollection as $data) {

			$collection[] = $this->toEntity($data);
		}
		return $collection;

	}

	public function toEntity(array $data): object
	{
		$this->parseXmlMappingStructure();
		return (new EntityCreator($this->structure))->create($data,$this->commandFqns,$this->creationMethod);

	}



	public function setCreationEntityMethod(string $method): void
	{
		$this->creationMethod = $method;
	}


	public function mapToStringStatus(): bool
	{
		return $this->mapAllToString;
	}

}