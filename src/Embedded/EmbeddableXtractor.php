<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Embedded;

use Insidesuki\EntityMapping\Fields\Field;
use Insidesuki\EntityMapping\Structure;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Xtractor for embeddables
 */
class EmbeddableXtractor
{

	public function __construct(
		private Structure $structure){}


	public function __invoke():void
	{

		$embededes = $this->structure->crawler()->filter('entity > embedded');
		foreach ($embededes as $embedded) {

			$emb      = new Crawler($embedded);
			$embField = new Embedded($emb->attr('name'), $emb->attr('class'));
			$this->structure->addToEmbeddable($embField);

		}

		$structureEmbeddables = $this->structure->embeddables();

		foreach ($structureEmbeddables as $embStructure) {

			$embedeables = $this->structure->crawler()->filter('embeddable');
			foreach ($embedeables as $embeded) {

				$embedCrawler = new Crawler($embeded);
				if($embedCrawler->attr('name') === $embStructure->fqnsClass) {

					$fields = $embedCrawler->filter('field');
					foreach ($fields as $field) {

						$fieldCrawler = new Crawler($field);
						$fieldMapped = Field::embedded(
							$fieldCrawler->attr('name'),
							$fieldCrawler->attr('column'),
							$embStructure->fqnsClass,
							$fieldCrawler->attr('type')
						);
						$embStructure->addField($fieldMapped);
						// add as column mapped
						$this->structure->addToColumnsMapped($fieldMapped);
					}


				}

			}


		}
	}
}