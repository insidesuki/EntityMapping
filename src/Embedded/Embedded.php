<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Embedded;

use Insidesuki\EntityMapping\Fields\Field;

/**
 *
 */
class Embedded
{


	public readonly string $fqnsClass;
	public readonly string $name;
	public array           $fields;

	/**
	 * @param string $name
	 * @param string $fqnsClass
	 * @param string $column
	 */
	public function __construct(
		string $name,
		string $fqnsClass
	)
	{
		$this->name = $name;
		$this->fqnsClass = $fqnsClass;
	}

	public function addField(Field $field): void
	{
		$this->fields[] = $field;
	}

	public function fields(): array
	{
		return $this->fields;
	}


}