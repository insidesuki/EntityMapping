<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Fields;
class Field
{


	public readonly string  $name;
	public readonly ?string $type;
	public readonly string  $column;
	public readonly bool    $id;
	public readonly string  $category;
	public readonly ?string $fqns;


	private function __construct(
		string $name,
		string $column,
		string $category,
		?string $fqns,
		?string $type,
		bool $id = false,
	)
	{

		$this->name     = $name;
		$this->column   = $column;
		$this->type     = $type;
		$this->id       = $id;
		$this->category = $category;
		$this->fqns     = $fqns;

	}


	public static function id(
		string $name,
		string $column,
		?string $type): self
	{

		return new self($name, $column, 'ID', null, $type, true);
	}


	public static function field(
		string $name,
		string $column,
		?string $type
	): self
	{


		return new self($name, $column, 'field', null, $type);
	}

	public static function embedded(string $name, string $column, string $fqns, ?string $type): self
	{

		return new self($name, $column, 'embedded', $fqns, $type);

	}


}