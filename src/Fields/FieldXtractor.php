<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping\Fields;

use Insidesuki\EntityMapping\Structure;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\DomCrawler\Crawler;

class FieldXtractor
{

	public function __construct(private Structure $structure){}

	public function __invoke(): void
	{

		// xtract id
		$this->xtractId();
		$fields = $this->structure->crawler()->filter('entity > field');
		foreach ($fields as $node) {
			$fieldCrawler = new Crawler($node);
			$fieldNode    = $fieldCrawler->filter('field');
			$field        = Field::field(
				$fieldNode->attr('name'),
				$fieldNode->attr('column'),
				$fieldNode->attr('type')
			);
			$this->structure->addToColumnsMapped($field);

		}


	}


	private function xtractId()
	{

		try {
			$id                       = $this->structure->crawler()->filter('id');
			$field                    = Field::id(
				$id->attr('name'),
				$id->attr('column'),
				null,
			);
			$this->structure->addToColumnsMapped($field);
			$this->structure->addId($field->name);
		}
		catch (RuntimeException $ex) {
			throw new InvalidArgumentException('Id mapping not found');
		}

	}

}