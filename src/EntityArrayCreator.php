<?php
declare(strict_types = 1);

namespace Insidesuki\EntityMapping;
class EntityArrayCreator
{

	public function __construct(private Structure $structure){}

	public function create(object $entity): array
	{

		$columns = $this->structure->columnsMapped();
		foreach ($columns as $field) {

			$method = $field->name;
			if($field->category === 'embedded') {

				// get embeddables
				$embeddables = $this->structure->embeddables();

				//dd($embeddables);
				foreach($embeddables AS $embedded){

					$nameEmb = $embedded->name;


					foreach ($embedded->fields() AS $f){

						$voMethod = $f->name;

						//dd($voMethod);
						$data[$f->column] = $entity->$nameEmb()->__toString();
					}
				}

			} else {

				$data[$field->column] = $entity->$method();
			}

		}

		return $data;
	}

}
